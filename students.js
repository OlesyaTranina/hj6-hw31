var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
/*1. задание*/
var students = studentsAndPoints.filter(function(item,i) {
  return !(i % 2);
});
var points = studentsAndPoints.filter(function(item,i) {
  return i % 2;
});
/*2. задание*/
console.log('Список студентов:');

students.forEach(function(item,i){
console.log('Cтудент %s  набрал %d баллов',item,points[i]);
});
/*3.задание*/
var maxIndex = 0;
var maxPoint = points.reduce(function(max,item,index){
  if (max<item) {
    max = item; 	
    maxIndex = index
  };
  return max;
}, 0);

console.log('');
console.log('Cтудент набравший максимальный балл: %s (%d баллов)',students[maxIndex],maxPoint);

/*4.задание*/
points = points.map(function(item,index){
if ((students[index]=='Ирина Овчинникова')||(students[index]=='Александр Малов')){
 return item +=30;
} else {
 return item;
}
});

/*5.задание*/
function compare(a,b){
	return b - a;
}

function getTop(count){
	/*копирую  массивы, что бы сохранить исходные данные*/
	var tmpPoints = points.slice();
	var sPoints = points.slice();

	sPoints.sort(compare);
	var sStudents = sPoints.map(function(item){
		var i = tmpPoints.indexOf(item);
		delete tmpPoints[i];
    return     students[i];
	});

	/*отбираю в массив верхние элементы столько сколько запрошено*/
	sPoints = sPoints.slice(0,count);
	/*создаю результирующий массив*/
	return sPoints.reduce(function(prev,item,i){
		return prev.concat([sStudents[i]],[item]);
	},[]);
	
};

var topList = getTop(3);

console.log('Топ 3:');

topList.forEach(function(item,i){
	if ((i % 2)==0) {
		console.log('%s  - %d баллов',item,topList[i+1]);	
	}
});

topList = getTop(5);

console.log('Топ 5:');

topList.forEach(function(item,i){
	if ((i % 2)==0) {
		console.log('%s  - %d баллов',item,topList[i+1]);	
	}
});